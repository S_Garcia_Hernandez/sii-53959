//AUTOR SERGIO GARCÍA

// MundoCliente.h: interface for the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Disparo.h"
#include "Socket.h"

using namespace std;

class CMundoCliente  
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	vector<Plano> paredes;
	vector<Esfera*> esferas;
	vector<Disparo*> disparos;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	DatosMemCompartida datosCompartidos;	//atributo de datos memoria compartida
	DatosMemCompartida *pdatosCompartidos; //puntero a datos memoria compartida
	
	
	int puntos1;
	int puntos2;
	
	Socket s_comunicacion_con_servidor;
	
	/*LA COMUNICACIÓN CLIENTE-SERVIDOR AHORA ES CON SOCKET
	
	//descriptor de fichero de la FIFO:SERVIDOR-CLIENTE
	int fifo_servidor_cliente;
	//descriptor de fichero de la FIFO:CLIENTE-SERVIDOR
	int fifo_cliente_servidor;
	
	*/
};

#endif // !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
