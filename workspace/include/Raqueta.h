// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
	friend class Disparo;
	public:
		int tamanoraqueta;
		Vector2D velocidad;
		Raqueta();
		virtual ~Raqueta();
		void Mueve(float t);
		bool recibeDisparo(Plano&);
		void agrandar(int n); //si n=1 agranda, si n=-1 disminuye
};
