//AUTOR SERGIO GARCÍA

// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Disparo.h"
#include "Socket.h"

using namespace std;

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();
	 
	Esfera esfera;
	vector<Plano> paredes;
	vector<Esfera*> esferas;
	vector<Disparo*> disparos;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	DatosMemCompartida datosCompartidos;	//atributo de datos memoria compartida
	DatosMemCompartida *pdatosCompartidos; //puntero a datos memoria compartida

	int puntos1;
	int puntos2;
	
	int fd;//tuberia FIFO LOGGER
	int fifo_servidor_cliente; //descriptor de fichero de la FIFO_SERVIDOR_CLIENTE
	int fifo_cliente_servidor; //descriptor de fichero de la FIFO_CLIENTE_SERVIDOR
	//identificador del thread
	pthread_t thid1;
	pthread_attr_t atrib;
	
	Socket s_conexion;
	Socket s_comunicacion_con_cliente;
	 
};

#endif // !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
