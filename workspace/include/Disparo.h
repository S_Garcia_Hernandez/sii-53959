
#include "Plano.h"
#include "Vector2D.h"

typedef enum{DERECHA,IZQUIERDA}sentido;

class Disparo: public Plano  {
	public:
		Vector2D velocidad;
		sentido s;
		Disparo(sentido s, Vector2D pos);
		virtual ~Disparo();
		void Mueve(float t);
		void Dibuja();
};
