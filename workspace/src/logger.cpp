#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string>
#include<iostream>
#include"Puntuaciones.h"

int main(){

int fd;

Puntuaciones puntos;

	/* crea el FIFO */

	if (mkfifo("/tmp/FIFO_LOGGER", 0600)<0) {
		perror("No puede crearse el FIFO_LOGGER");
		return 1;
	}

	/* Abre el FIFO */

	if ((fd=open("/tmp/FIFO_LOGGER", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO_LOGGER");
		return 1;
	}

	int errorread;

	//BUCLE INFINITO LEYENDO DEL FIFO_LOGGER
	while(errorread=read(fd, &puntos, sizeof(puntos))==sizeof(puntos))  {
		
		if(puntos.lastWinner==1){
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}

		else if(puntos.lastWinner==2){
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
	}
	if(errorread==0){
			printf("Error:estoy leyendo de una tuberia vacia");
			return -1;
			}
	close(fd);
	int errorfifo;
	errorfifo=unlink("/tmp/FIFO_LOGGER");
	if(errorfifo<0)
		printf("error al borrar la tuberia");
		
	return(0);

}

