// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include<iostream>
#include"Puntuaciones.h"
#include<string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <signal.h>

#define NMAXDISPAROS 10
int nDisparosJ1=0;
int nDisparosJ2=0;
int nesferas=1;
#define NMAXESFERAS 1
#define PERIODO 200
#define MAXGOLPES 5
int golpes1=0;
int golpes2=0;
bool bloqueo1=false;
bool bloqueo2=false;

//FUNCION PARA CERRAR EL SERVIDOR QUE SE EJECUTA CUANDO LLEGA LA SEÑAL SIGPIPE

void* hilo_comandos(void* d){
      CMundoServidor* p = (CMundoServidor*) d;
      p->RecibeComandosJugador();
}

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
     /*LA COMUNICACIÓN con el cliente ya NO SE HACE CON FIFOS */
     
	//cerramos tuberia
	close (fd);
	unlink("/tmp/FIFO_LOGGER");
	
	/*
	close (fifo_servidor_cliente);
	unlink("/tmp/FIFO_SERVIDOR_CLIENTE");
	close (fifo_cliente_servidor);
	unlink("/tmp/FIFO_CLIENTE_SERVIDOR");
	*/
	
	
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
		
	for(i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();
		
	for(i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();
		
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	
	if( puntos1>5 &&  puntos2<5){
		printf("Gana Jugador 1: %d\n",puntos1);
		exit(0);

	}
	if( puntos2>5 &&  puntos2<5){
		printf("Gana jugador 2 :%d\n",puntos2);
		exit(0);
	}

	//AQUI TERMINA MI DIBUJO
	

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	static int timer=0;
	Puntuaciones puntos;//estructura para que el logger lea los datos de puntuacion 
	
	//si  no hay impacto de disparos
	if(!bloqueo1)
 		jugador1.Mueve(0.025f);
	if(!bloqueo2)
 		jugador2.Mueve(0.025f);
 		
	//movimiento esferas
	esfera.Mueve(0.025);
	for(int i=0;i<esferas.size();i++){
		esferas[i]->Mueve(0.025f);
	}	
	//movimiento disparos
	for(int i=0;i<disparos.size();i++){
		disparos[i]->Mueve(0.025f);
	}
	//rebote  con paredes
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	//rebote esferas
	for(int j=0;j<esferas.size();j++){
		for(int i=0;i<paredes.size();i++){
			paredes[i].Rebota(*esferas[j]);
		}
	}
	//gestión de los disparos
	for(int j=0;j<disparos.size();j++){
		bool destruirDisparo=false;
	
		if(jugador1.recibeDisparo(*disparos[j])){
			jugador1.agrandar(-1);	
			jugador1.velocidad.y=0;
			bloqueo1=true;
 			destruirDisparo=true;
 			nDisparosJ2-=1;
 		}
		
		if(jugador2.recibeDisparo(*disparos[j])){
			jugador2.agrandar(-1);
			jugador2.velocidad.y=0;	
			bloqueo2=true;
 			destruirDisparo=true;
 			nDisparosJ1-=1;
 		}
		
		if(fondo_izq.Rebota(*disparos[j])||fondo_dcho.Rebota(*disparos[j])){
			if(disparos[j]->s==DERECHA){nDisparosJ1-=1;}
			if(disparos[j]->s==IZQUIERDA){nDisparosJ2-=1;}
			destruirDisparo=true;		
		}

		if(destruirDisparo){
			delete disparos[j];
			disparos.erase(disparos.begin()+j);
			disparos.shrink_to_fit();
		}
	}
	
	// agranda la raqueta con el tiempo
	if(timer%PERIODO==0){//resto de la division
		jugador1.agrandar(1);
		jugador2.agrandar(1);
		timer=0;
		bloqueo1=false;
		bloqueo2=false;
	}
	//añadir esferas con el tiempo
	if(timer%PERIODO==0){
		if( nesferas < NMAXESFERAS){
			esferas.push_back(new Esfera);
			nesferas++;
		}
	}
	
	//rebote esferas en raquetas
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	for(int i=0;i<esferas.size();i++){
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);	
	}

	//gestión de los puntos
	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWinner=2;
		write(fd, &puntos, sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{	
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWinner=1;
		write(fd, &puntos, sizeof(puntos));
	}
	for(int i=0;i<esferas.size();i++){

		if(fondo_izq.Rebota(*esferas[i])){
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			
			//comunicacion con fifo
			puntos.jugador1=puntos1;
			puntos.jugador2=puntos2;
			puntos.lastWinner=2;
			write(fd, &puntos, sizeof(puntos));
		}

		if(fondo_dcho.Rebota(*esferas[i])){	
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos1++;
			
			//comunicacion fifo
			puntos.jugador1=puntos1;
			puntos.jugador2=puntos2;
			puntos.lastWinner=1;
			write(fd, &puntos, sizeof(puntos));
		}				
	}
	timer+=1;
	
	//CREAMOS LA CADENA QUE SE ENVIA DEL SERVIDOR AL CLIENTE CON LA INFO DE LAS POSICIONES DE LA ESFERA JUGADORES Y PUNTOS
	
	
	char cad[200];
		sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d",
			esfera.centro.x,esfera.centro.y,esfera.radio,
			jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
			jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
			puntos1, puntos2
		);
		
	//write(fifo_servidor_cliente,cad,sizeof(cad));
	//El servidor envía las coordenadas a través del socket al cliente en el OnTimer.
	s_comunicacion_con_cliente.Send(cad,sizeof(cad));

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	
}


void CMundoServidor::Init()
{
	//INICIALIZACION DE LA FIFO LOGGER

	if ((fd=open("/tmp/FIFO_LOGGER", O_WRONLY))<0) { //directorio para archivos temporales
		perror("error al abrir FIFO_LOGGER");
		return ;
	}

	/*LA CONEXION AHORA ES POR SOCKETS ENTRE CLIENTE-SERVIDOR
	
	//APERTURA FIFO SERVIDOR_CLIENTE CLIENTE EN MODO ESCRITURA
	
	fifo_servidor_cliente=open("/tmp/FIFO_SERVIDOR_CLIENTE",O_WRONLY);
	if(fifo_servidor_cliente<0){
		perror("No puede abrirse el FIFO_SERVIDOR_CLIENTE");
		return;
	}
	
	*/
	
	//CONEXIÓN POR SOCKETS
	//El servidor enlaza el socket de conexión a una dirección IP y un puerto (siendo la IP la de la máquina en la que corre el servidor).
	char ip[]="127.0.0.1";

	if(s_conexion.InitServer(ip,4200)==-1)
		printf("error abriendo el servidor\n");

	s_comunicacion_con_cliente=s_conexion.Accept();//devuelve el socket para la comunicación con el cliente, que es por el que se envían y reciben los datos.

	//El servidor recibe el nombre del cliente conectado a través del socket y lo imprime por pantalla.
	char nombre[50];
	s_comunicacion_con_cliente.Receive(nombre,sizeof(nombre));
	printf("%s se ha conectado a la partida.\n",nombre);
	
	
	//CREAR EL THREAD INDICANDOLE LA FUNCIÓN A AJECUTAR
	//El hilo que recibe los comandos del cliente es el que recibe los datos de las teclas a través del socket.
	pthread_create(&thid1, NULL, hilo_comandos, this);
	
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}



//el método RecibeComandosJugador() hace un bucle infinito donde, en cada iteración, lee de la tubería la tecla pulsada y modifica la velocidad del jugador correspondiente.

void CMundoServidor::RecibeComandosJugador(){
 	/*
 	
	//APERTURA FIFO CLIENTE SERVIDOR EN MODO LECTURA

	fifo_cliente_servidor=open("/tmp/FIFO_CLIENTE_SERVIDOR",O_RDONLY);
	if(fifo_cliente_servidor<0){
		perror("No puede abrirse el FIFO_CLIENTE_SERVIDOR");
		return ;
	}
	
	*/

       while (1) {
            usleep(10);
            char cad[100];
            //read(fifo_cliente_servidor, cad, sizeof(cad));
            s_comunicacion_con_cliente.Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	    if(key=='d'){
		if(nDisparosJ1<NMAXDISPAROS){
			disparos.push_back(new Disparo(DERECHA,jugador1.getCentro()));
			nDisparosJ1+=1;
		}	
	    }
	    if(key=='k'){
		if(nDisparosJ2<NMAXDISPAROS){
			disparos.push_back(new Disparo(IZQUIERDA,jugador2.getCentro())); 
			nDisparosJ2+=1;
		}
	    }
      }

}

