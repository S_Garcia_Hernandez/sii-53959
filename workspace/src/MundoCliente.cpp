// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include<iostream>
#include"Puntuaciones.h"
#include<string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>

#define NMAXDISPAROS 10
#define NMAXESFERAS 2
#define PERIODO 200
#define MAXGOLPES 5
//variables globales
int nDisparosJ1=0;
int nDisparosJ2=0;
int golpes1=0;
int golpes2=0;
bool bloqueo1=false;
bool bloqueo2=false;


CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	
	munmap(pdatosCompartidos,sizeof(datosCompartidos));
	unlink("datosCompartidos");
	
	/*LA COMUNICACIÓN CLIENTE-SERVIDOR AHORA ES CON SOCKET
	
	//cierre de tuberias
	close(fifo_servidor_cliente);
	unlink("/tmp/FIFO_SERVIDOR_CLIENTE");
	
	close(fifo_cliente_servidor);
	unlink("/tmp/FIFO_CLIENTE_SERVIDOR");
	
	*/
	
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
		
	for(i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();
		
	for(i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();
		
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	if( puntos1>5 &&  puntos2<5){
		printf("Gana Jugador 1: %d\n",puntos1);
		exit(0);

	}
	if( puntos2>5 &&  puntos2<5){
		printf("Gana jugador 2 :%d\n",puntos2);
		exit(0);
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	
	
	//LEEMOS LOS DATOS ENVIADOS POR EL SERVIDOR
	char cad[200];
	
	/*LA COMUNICACIÓN CLIENTE-SERVIDOR AHORA ES CON SOCKET
	
	read(fifo_servidor_cliente,cad,sizeof(cad));
	
	*/
	
	//El cliente recibe las coordenadas a través del socket en el OnTimer.
	s_comunicacion_con_servidor.Receive(cad, sizeof(cad));
	
	sscanf(
		cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", 
		&esfera.centro.x,&esfera.centro.y,&esfera.radio,
		&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
		&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, 
		&puntos1, &puntos2
	); 
	
	
	
	//Actualizacion de los campos del atributo DatosCompartidos e invocar al metodo onekeyboarddown

	pdatosCompartidos->esfera=esfera;
	pdatosCompartidos->raqueta1=jugador1;
	
	if(pdatosCompartidos->accion==-1){
		OnKeyboardDown('s', 0, 0);
	}

	else if(pdatosCompartidos->accion==0){}

	else if(pdatosCompartidos->accion==1){
	OnKeyboardDown('w', 0, 0);
	}


}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
 	//El cliente Escribe los datos en la tubería 
	switch(key)
	{
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w"); break;
	case 'l':sprintf(tecla,"l");break;
	case 'o':sprintf(tecla,"o");break;
	case 'd':sprintf(tecla,"d");break;
	case 'k':sprintf(tecla,"d");break;
	}
	
	//El cliente envía la tecla a través del socket en el método OnKeyboardDown.
	
	/*LA COMUNICACIÓN CLIENTE-SERVIDOR AHORA ES CON SOCKET
	
 	write(fifo_cliente_servidor,&tecla,sizeof(tecla));
 	
 	*/
 	
 	s_comunicacion_con_servidor.Send(tecla,sizeof(tecla));
 }


void CMundoCliente::Init()
{
	
	//CREACION DEL FICHERO MEMCOMPARTIDA Y MMAP

	int fd_mmap;
	char *pfd;
	fd_mmap= open("/tmp/datosCompartidos", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0) {
          perror("Error creación fichero destino");
           exit(1);
        }
	datosCompartidos.esfera=esfera;
	datosCompartidos.raqueta1=jugador1;
	datosCompartidos.accion=0;
	
	write(fd_mmap,&datosCompartidos,sizeof(datosCompartidos));

	pdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosCompartidos),PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0));

	if (pdatosCompartidos==MAP_FAILED){
		perror("Error en la proyeccion del fichero origen");
		close(fd_mmap);
		return;
	}
	close(fd_mmap);

	//Fin de la proyeccion en memoria

/*LA COMUNICACIÓN CLIENTE-SERVIDOR AHORA ES CON SOCKET

	//El cliente crea la tubería con nombre la abre en modo lectura (mediante mkfifo y open).
	
	mkfifo("/tmp/FIFO_SERVIDOR_CLIENTE",0600); //110 000 000
	fifo_servidor_cliente=open("/tmp/FIFO_SERVIDOR_CLIENTE",O_RDONLY); 
	if(fifo_servidor_cliente<0){
		perror("Error en la creacion de FIFO_SERVIDOR_CLIENTE");
		return ;
	}
	
	//CREACION Y APERTURA EN MODO ESCRITURA FIFO CLIENTE SERVIDOR
	mkfifo("/tmp/FIFO_CLIENTE_SERVIDOR",0600);
	fifo_cliente_servidor=open("/tmp/FIFO_CLIENTE_SERVIDOR",O_WRONLY);
	if(fifo_cliente_servidor<0){
		perror("Error en la creacion de FIFO_CLIENTE_SERVIDOR");
		return ;
	}
*/	
	
	//CONEXION CON SERVIDOR
	
	char ip[]="127.0.0.1";
	char nombre[50];
	//El cliente pide por teclado un nombre para enviárselo al servidor.
	printf("Introduzca su nombre:\n");
	scanf("%s",nombre);
	//El cliente se conecta a la IP y puerto del servidor.
	s_comunicacion_con_servidor.Connect(ip,4200);
	//Envía el nombre a través del socket.
	s_comunicacion_con_servidor.Send(nombre,sizeof(nombre));
	
	
	
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

